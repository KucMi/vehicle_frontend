"use client";

import {useEffect, useState} from "react";

type Vehicle = {
    name: string;
    vehicle_name: string;
    weight: string;
};

export default function Dashboard() {
    const [currentVehicle, setCurrentVehicle] = useState<Vehicle | null>(null);


    useEffect(() => {
        (async (variable) => {
            const result = await fetch("http://localhost:8000/api/v1/vehicles/3");
            const vehicle = await result.json();

            setCurrentVehicle(vehicle);
        })();
    }, []);

    return (
        <div>
            {currentVehicle ? (
                <>
                    <h1>Vozidlo {currentVehicle.vehicle_name}</h1>
                </>
            ) : (
                <div>Pujdes pesky, nemas vozidlo</div>
            )}
        </div>
    );
}