"use client";

import NextLink from "next/link";

export default function LoginPage() {
    return (
        <>
            <h1>Login page</h1>

            <NextLink className={"underline"} href="/vehicle/create">Don&apos;t have an account?</NextLink>
        </>
    );
}