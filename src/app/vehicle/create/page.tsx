"use client";

import NextLink from "next/link";
import {useEffect, useState} from "react";
import {useRouter} from "next/navigation";

type FormValues = {
    name: string;
    vehicle_name: string;
    weight: string;
};

export default function VehiclePage() {
    const router = useRouter();

    const [formData, setFormData] = useState<FormValues>({
        name: "",
        vehicle_name: "",
        weight: "",
    });

    useEffect(() => {

    }, []);

    function isFormValid() {
        return Object.values(formData).every((value) => {
            return value;
        });
    }

    function vehicleNameChanged(value: string) {
        setFormData({
            ...formData,
            vehicle_name: value,
        });
    }

    function nameChanged(value: string) {
        setFormData({
            ...formData,
            name: value,
        });
    }

    function weightChanged(value: string) {
        setFormData({
            ...formData,
            weight: value,
        });
    }

    async function formSubmitted(e: React.MouseEvent<HTMLFormElement>) {
        e.preventDefault();

        await fetch("http://localhost:8000/api/v1/vehicles", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({...formData, weight: Number(formData.weight)}),
        });

        await router.push("/vehicle/login");
    }

    return (
        <>
            <h1 className={"text-3xl md:text-5xl font-extrabold mb-4"}>Create a vehicle</h1>

            <form
                className={"flex gap-6 flex-col max-w-md w-full"}
                onSubmit={formSubmitted}
            >
                <div>
                    <label htmlFor="vehicle_name"
                           className="block mb-2 text-sm font-medium text-gray-900">Vehicle name</label>
                    <input type="text"
                           id="vehicle_name"
                           value={formData.vehicle_name}
                           onChange={(e) => vehicleNameChanged(e.target.value)}
                           className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                           required/>
                </div>

                <div>
                    <label htmlFor="name"
                           className="block mb-2 text-sm font-medium text-gray-900">Name</label>
                    <input type="text"
                           id="name"
                           value={formData.name}
                           onChange={(e) => nameChanged(e.target.value)}
                           className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                           required/>
                </div>

                <div>
                    <label htmlFor="weight"
                           className="block mb-2 text-sm font-medium text-gray-900">Weight</label>
                    <input type="number"
                           id="weight"
                           value={formData.weight}
                           onChange={(e) => weightChanged(e.target.value)}
                           className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                           required/>
                </div>

                <button type="submit"
                        disabled={!isFormValid()}
                        className="disabled:opacity-70 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5">Submit
                </button>

                <NextLink className={"underline"} href="/vehicle/login">Already registered?</NextLink>
            </form>
        </>
    );
}
