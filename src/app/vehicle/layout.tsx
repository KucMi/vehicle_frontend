import {ReactNode} from "react";

type Props = {
    children: ReactNode;
};

export default function Layout({ children }: Props) {
    return (
        <div className={"grid place-items-center p-8"}>
            {children}
        </div>
    );
}